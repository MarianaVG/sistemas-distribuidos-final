
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.types.FacebookType;
import com.restfb.types.User;
import java.io.*;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;
import javax.xml.bind.DatatypeConverter;
import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MultiServerThread extends Thread {
    private Socket socket = null;

    public void init() {
    }

    public MultiServerThread(Socket socket) {
        super("MultiServerThread");
        this.socket = socket;
        ServerMultiClient.NoClients++;
    }
    public void run() {
        init();
        try {
            BasicWebCrawler clima = new BasicWebCrawler();
            Conversion_dolar dolar = new Conversion_dolar();
            Scanner teclado = new Scanner(System.in);
            PrintWriter escritor = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String lineIn, lineOut;
            
            char nuevacadena[];

            while ((lineIn = entrada.readLine()) != null) {
                nuevacadena = lineIn.toCharArray();

                int num;
                num = lineIn.length();
                
                if (lineIn.isEmpty()) {
                   escritor.print(".");                   
                } else {
                    if (lineIn.equals("FIN")) {
                        ServerMultiClient.NoClients--;
                        break;
                    } else if (lineIn.equals("#cuantos#")) {
                        escritor.println("Clientes conectados: " + ServerMultiClient.NoClients);
                    } else if(lineIn.charAt(0)=='#'){//Verifica el incio del servicio
                        if(lineIn.charAt(lineIn.length()-1)=='&'){//Verifica el final del servicio
                        //Primero se verifican el inicio y fin para evitar errores de sintaxis
                            if(lineIn.charAt(1)=='5' && lineIn.charAt(2)=='7' && lineIn.charAt(3)=='4'){
                                //Verifica el codigo del servicio
                                //Separa la cadena (de cadenas) de los caracteres que especifican que es un servicio
                                String subLine = lineIn.substring(7, lineIn.length()-1);
                                //Divide las cadenas cada que encuentra un caracter "|"
                                String[] subCadenas = subLine.split("\\|");
                                if(Integer.parseInt(String.valueOf(lineIn.charAt(5))) == subCadenas.length){
                                     //Se verifica que el numero de cadenas este correcto
                                    ArrayList<String> listHex = new ArrayList<>();
                                    //Convierte las cadenas a hexadecimal
                                    for (String subCadena : subCadenas) {
                                        listHex.add(toHexadecimal(subCadena));
                                    }
                                    //Lo pasamos a un array para poder concatenarlo
                                    String[] hexCadenas = listHex.toArray(new String[listHex.size()]);
                                    //Se concatenan las cadenas, todas separadas por un "|" (ver funcion)
                                    String cadenaConcatenada = convertArrayToString(hexCadenas);
                                     //Cadena de Salida
                                    escritor.println("#R-574|"+lineIn.charAt(5)+"|"+cadenaConcatenada+"&");
                                    escritor.flush();
                                }else{
                                    escritor.println("ERROR! ->numero de cadenas incorrecto<-");
                                    escritor.flush();
                                }
                            }else{
                                escritor.println("ERROR! ->codigo incorrecto<-");
                                escritor.flush();
                            }
                        }else{
                            escritor.println("ERROR! ->sintaxis incorrecta<-");
                            escritor.flush();
                        }
                    } else if(lineIn.charAt(0)=='#' && lineIn.charAt(1)=='1' && lineIn.charAt(2)=='|'){//Encripta una cadena
                        //CADENA EJEMPLO PARA USAR EL ENCRIPTADO: #1|Hola
                        String subLine = lineIn.substring(3, lineIn.length());
                        String cadenaEncriptada = encriptar(subLine);

                        escritor.println("CADENA ENCRIPTADA: "+cadenaEncriptada);
                        escritor.flush();  
                    } else if(lineIn.charAt(0)=='#' && lineIn.charAt(1)=='2' && lineIn.charAt(2)=='|'){//Desencripta una cadena
                        //CADENA EJEMPLO PARA USAR EL DESENCRIPTADO: #2|SG9sYQ==
                        String subLine = lineIn.substring(3, lineIn.length());
                        String cadenaDesencriptada = desencriptar(subLine);

                        escritor.println("CADENA DESENCRIPTADA: "+cadenaDesencriptada);
                        escritor.flush();
                    } else if (nuevacadena[0] == '#' && nuevacadena[1] == '1' && nuevacadena[2] == '4' && nuevacadena[3] == '5' && nuevacadena[4] == '|' && nuevacadena[5] == '1' && nuevacadena[6] == '|' && nuevacadena[num - 1] == '&') {
                        
                        clima.getPageLinks("https://www.meteored.mx/clima_Nueva+York-America+Norte-Estados+Unidos-Livingston-KNYC-1-11290.html");
                        int num2 = clima.prueba.temperatura.length();
                        //escritor.println(clima.prueba.clima);
                        escritor.print("el valor de la tempratura es:  ");
                        for (int i = 56; i < (num2 - 7); i++) {
                            escritor.print(clima.prueba.temperatura.charAt(i));

                        }
                       escritor.println("C");
                       
                    } else if (lineIn.equals("#622|1|dolar|&")) {
                    escritor.print("CONVERSIÓN PESOS MEXICANOS A DOLAR");
                        dolar.getPageLinks("https://mx.investing.com/currencies/usd-mxn-converter");
                        escritor.println();
                        escritor.print(" el tipo de cambio promedio del dólar en México es de " + dolar.prueba.conversion);
                      escritor.println();
                    } else if (lineIn.equals("#622|1|fb|&")) {
                        String r;
                        r = "ayuda";
                        HttpResponse re;
                        String accessToken = "EAAekdO1RMFIBADWrbwUIcCQsfaUAGtVipk4RsHL5oNZCE6CY6Qc5XYY25pJLcwbe2N07XPLA8TD7zj3oVKk85mbxIzDUOuLnJ6c6A2GVtVTFlcX8Gm2wqicQOByPVmvIjP2ri065PMkOnc26oChej35p0WzZAiw7CpHVNFj797hgtwh6ALNfEkxs54DYQZD";
                        re = Request.Post("https://graph.facebook.com/111586106909285/feed/").bodyForm(Form.form().add("message", r).add("access_token", accessToken).build()).execute().returnResponse();
           
                         escritor.println("Publicado");
                    }
                }
                /*AQUI FINALIZA ENCRIPTACION*/
                escritor.print(" ");
            }

            escritor.println("c");
            try {
                entrada.close();
                escritor.close();
                socket.close();
            } catch (Exception e) {
                System.out.println("123456");
                System.out.println("Error 2: " + e.toString());
                System.out.println("1");
                socket.close();
                System.exit(0);
            }
        } catch (IOException e) {
            System.out.println("2");
            e.printStackTrace();

        }
    }

    private static String encriptar(String s) throws UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(s.getBytes("utf-8"));
    }

    private static String desencriptar(String s) throws UnsupportedEncodingException {
        byte[] decode = Base64.getDecoder().decode(s.getBytes());
        return new String(decode, "utf-8");
    
    }
    
    public static String toHexadecimal(String text) throws UnsupportedEncodingException{
        byte[] myBytes = text.getBytes("UTF-8");
        return DatatypeConverter.printHexBinary(myBytes);
    }
    
    public static String convertArrayToString(String[] strArray) {
        String joinedString = String.join("|", strArray);
        return joinedString;
    }


}
